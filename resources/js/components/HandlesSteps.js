export default {
  data: () => ({
    fields: [],
    panels: [],
    step: 0,
    lastCompletedStep: 0,
    loadingSteps: false
  }),

  watch: {
    'step': function(to, from) {
      if (to !== this.$route.query.step) {
        this.$router.push({query: {step: to}})
      }
    }
  },

  methods: {
    initializeStep() {
      if (this.$route.query.step) {
        this.step = this.$route.query.step
      }
      if(this.lastStep > this.step) {
        this.step = this.lastStep;
      }
      if(this.step < 0) {
        this.step = 0;
      }
    },

    async handlePrevious() {
      this.updateLastCompletedStep(this.step)
      this.step && --this.step

      await this.getFields()
    },

    async handleJumpToStep(index) {
      this.updateLastCompletedStep(this.step)
      this.step = index

      await this.getFields()
    },

    updateLastCompletedStep(step) {
      if(step && step > this.lastCompletedStep) {
        this.lastCompletedStep = step
      }
    },
  },

  computed: {
    lastStep() {
      return this.$route.params.step;
    },

    currentPanel() {
      return this.panelsWithFields[this.step];
    },

    panelsWithFields() {
      return _.map(this.panels, panel => {
        return {
          ...panel,
          fields: _.filter(this.fields, field => field.panel == panel.name),
        }
      }).filter(panel => panel.step != undefined)
    },
  },
}
