<?php

namespace BoiteBeet\NovaWizard;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;
use Laravel\Nova\Nova;

class ToolServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'wizard');

        $this->publishes([
            __DIR__ . '/../config/nova-steps.php' => config_path('nova-steps.php'),
        ], 'config');

        $this->mergeConfigFrom(__DIR__ . '/../config/nova-steps.php', 'nova-steps');

        $this->app->booted(function () {
            $this->routes();
            Nova::serving([$this, 'servingNova']);
        });
    }

    /**
     * Regsiter the Nova application manifests.
     *
     * @return void
     */
    public function servingNova()
    {
        Nova::provideToScript([
            'wizard' => [
                'resources' => collect(Nova::$resources)->map(function($resource) {
                    if(is_subclass_of($resource, Contracts\Wizard::class)) {
                        return [
                            'key'  => $resource::uriKey(),
                            'step' => 0,
                            'update' => ! is_subclass_of($resource, Contracts\IgnoreUpdateWizard::class),
                            'navigable' => is_subclass_of($resource, Contracts\Navigable::class),
                            'config' => method_exists($resource, 'wizardConfig') ?
                                $resource::wizardConfig() :
                                config('nova-steps')
                        ];
                    }
                })->filter()->values()
            ]
        ]);

        Nova::script('nova-form-step', __DIR__.'/../dist/js/tool.js');
        Nova::style('nova-form-step', __DIR__.'/../dist/css/tool.css');
    }

    /**
     * Register the tool's routes.
     *
     * @return void
     */
    protected function routes()
    {
        if ($this->app->routesAreCached()) {
            return;
        }

        Route::middleware(['nova'])
                ->namespace(__NAMESPACE__.'\\Http\\Controllers')
                ->prefix('nova-api')
                ->group(__DIR__.'/../routes/api.php');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
