/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./resources/**/*.js",
    './resources/js/**/*.vue',
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}
