<?php

return [

    'buttons' => [
        'visibility' => [
            'cancel'          => true,
            'previous'        => true,
            'next'            => true,
            'saveAndContinue' => true,
            'saveAndQuit'     => true,
        ],
        'labels'     => [
            'cancel'          => 'Cancel',
            'previous'        => 'Previous',
            'next'            => 'Next',
            'saveAndContinue' => [
                'create' => 'Create :resource',
                'update' => 'Update :resource'
            ],
            'saveAndQuit'     => [
                'create' => 'Create & Add Another',
                'update' => 'Update & Continue Editing'
            ],
        ]
    ],

    'warning' => [
        'show'    => false,
        'message' => ''
    ]

];
